package edu.doane.sudoku.view.desktop;

import edu.doane.sudoku.controller.SuDoKuController;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Class handling menu actions for the desktop Doane SuDoKu app. 
 * 
 * @author Mark M. Meysenburg
 * @version 12/16/2015
 */
public class UIMenuHandler implements ActionListener {
    
	/**
	 * Reference to the app's controller
	 */
    private SuDoKuController controller;
    
    /**
     * Construct the menu handler. 
     * 
     * @param controller Reference to the app's controller.
     */
    public UIMenuHandler(SuDoKuController controller) {
        this.controller = controller;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
    	// depending on menu selected, ask controller to perform actions
        String menuItem = e.getActionCommand();
        if(menuItem.equals("Exit")) {
            controller.shutDown();
        } else if(menuItem.equals("New game")) {
            controller.requestGame(null);
        } else if(menuItem.equals("About...")) {
            controller.displayAbout();
        } else if(menuItem.equals("Clear grid")) {
            controller.resetGrids();
        }
    }
    
}
