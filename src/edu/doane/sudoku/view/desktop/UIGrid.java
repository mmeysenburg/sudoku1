package edu.doane.sudoku.view.desktop;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridLayout;
import javax.swing.JPanel;

/**
 * Custom JPanel representing a 3x3 grid of blocks in the desktop SuDoKu app. 
 * 
 * @author Mark M. Meysenburg
 * @version 12/16/15
 */
public class UIGrid extends JPanel {
    
	/**
	 * Silence the Eclipse warning about serialization, even though
	 * we're not doing that. 
	 */
	private static final long serialVersionUID = 1L;

	public UIGrid() {
        super();
        // grid has a black background and a gap between each block,
        // causing thick lines to show between each block 
        GridLayout gl = new GridLayout(3, 3);
        gl.setHgap(4);
        gl.setVgap(4);
        this.setLayout(gl);
        this.setBackground(Color.BLACK);
    }
    
    @Override
    public void paint(Graphics g) {
        super.paint(g);
    }
    
}
