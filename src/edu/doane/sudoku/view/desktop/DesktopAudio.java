package edu.doane.sudoku.view.desktop;

import java.io.IOException;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.JOptionPane;

/**
 * Singleton class providing simple audio for the desktop SuDoKu application. 
 * 
 * @author Mark M. Meysenburg
 * @version 02/08/2016
 *
 */
public class DesktopAudio {

	/**
	 * Singleton instance for the DesktopAudio object. 
	 */
	private static DesktopAudio instance = null;
	
	/**
	 * Media for toggling a note.
	 */
	private AudioInputStream noteToggleMedia;
	
	/**
	 * Media player for toggling a note.
	 */
	private Clip noteTogglePlayer;
	
	/**
	 * Media for playing a number.
	 */
	private AudioInputStream playNumberMedia;
	
	/**
	 * Media player for playing a number.
	 */
	private Clip playNumberPlayer;
	
	/**
	 * Media for erasing a number.
	 */
	private AudioInputStream eraseNumberMedia;
	
	/**
	 * Media player for erasing a number.
	 */
	private Clip eraseNumberPlayer;
	
	/**
	 * Media for clearing the grid.
	 */
	private AudioInputStream clearGridMedia;
	
	/**
	 * Media player for clearing the grid.
	 */
	private Clip clearGridPlayer;
	
	/**
	 * Media for celebrating a win. 
	 */
	private AudioInputStream celebrateMedia;
	
	/**
	 * Media player for celebrating a win. 
	 */
	private Clip celebratePlayer;
	
	/**
	 * Media for starting a new game. 
	 */
	private AudioInputStream newGameMedia;
	
	/**
	 * Media player for starting a new game. 
	 */
	private Clip newGamePlayer;
	
	/**
	 * Get the instance to the DesktopAudio object. 
	 * 
	 * @return Single instance of the DesktopAudio object.
	 */
	public static DesktopAudio getInstance() {
		if(instance == null) {
			instance = new DesktopAudio();
		}
		return instance;
	}
	
	/**
	 * Private constructor for the class. Loads the audio clips from the app's jar file. 
	 */
	private DesktopAudio() {
		try {
			// load number play sound
			playNumberMedia = AudioSystem.getAudioInputStream(ClassLoader.getSystemResource("resources/audio/play-number.wav"));
			playNumberPlayer = AudioSystem.getClip();
			playNumberPlayer.open(playNumberMedia);

			// load number erase sound
			eraseNumberMedia = AudioSystem.getAudioInputStream(ClassLoader.getSystemResource("resources/audio/erase-number.wav"));
			eraseNumberPlayer = AudioSystem.getClip();
			eraseNumberPlayer.open(eraseNumberMedia);
			
			// load clear grid sound
			clearGridMedia = AudioSystem.getAudioInputStream(ClassLoader.getSystemResource("resources/audio/clear-grid.wav"));
			clearGridPlayer = AudioSystem.getClip();
			clearGridPlayer.open(clearGridMedia);
			
			// load note toggling sound
			noteToggleMedia = AudioSystem.getAudioInputStream(ClassLoader.getSystemResource("resources/audio/note-toggle.wav"));
			noteTogglePlayer = AudioSystem.getClip();
			noteTogglePlayer.open(noteToggleMedia);
			
			// load celebration sound
			celebrateMedia = AudioSystem.getAudioInputStream(ClassLoader.getSystemResource("resources/audio/celebrate.wav"));
			celebratePlayer = AudioSystem.getClip();
			celebratePlayer.open(celebrateMedia);
			
			// load new game sound
			newGameMedia = AudioSystem.getAudioInputStream(ClassLoader.getSystemResource("resources/audio/new-game.wav"));
			newGamePlayer = AudioSystem.getClip();
			newGamePlayer.open(newGameMedia);
		} catch (UnsupportedAudioFileException | IOException | LineUnavailableException e) {
			JOptionPane.showMessageDialog(null, "Unable to load audio files!", "Audio Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	/**
	 * Play the note toggling sound. 
	 */
	public void playNoteToggle() {
		noteTogglePlayer.stop();
		noteTogglePlayer.setMicrosecondPosition(0L);
		noteTogglePlayer.start();
	}
	
	/**
	 * Play the play number sound. 
	 */
	public void playPlayNumber() {
		playNumberPlayer.stop();
		playNumberPlayer.setMicrosecondPosition(0L);
		playNumberPlayer.start();
	}

	/**
	 * Play the erase number sound. 
	 */
	public void playEraseNumber() {
		eraseNumberPlayer.stop();
		eraseNumberPlayer.setMicrosecondPosition(0L);
		eraseNumberPlayer.start();
	}
	
	/**
	 * Play the clear grid sound. 
	 */
	public void playClearGrid() {
		clearGridPlayer.stop();
		clearGridPlayer.setMicrosecondPosition(0L);
		clearGridPlayer.start();
	}
	
	/**
	 * Play the win game sound. 
	 */
	public void playCelebrate() {
		celebratePlayer.stop();
		celebratePlayer.setMicrosecondPosition(0L);
		celebratePlayer.start();
	}
	
	/**
	 * Play the new game sound. 
	 */
	public void playNewGame() {
		newGamePlayer.stop();
		newGamePlayer.setMicrosecondPosition(0L);
		newGamePlayer.start();
	}
}
